#!/bin/bash

安装 JDK
sudo apt update
sudo apt install openjdk-8-jdk -y

# MYSQL_OLD_CONF="# db.url.0=jdbc:mysql://127.0.0.1:3306/nacos?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000"
# MYSQL_NEW_CONF="db.url.0=jdbc:mysql://localhost:3306/nacos_config?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000"

MYSQL_OLD_CONF="# db.url.0=jdbc:mysql:"
MYSQL_NEW_CONF="db.url.0=jdbc:mysql:"


# 下载并解压缩 Nacos 发行版
wget https://github.com/alibaba/nacos/releases/download/2.0.3/nacos-server-2.0.3.tar.gz
tar -xvf nacos-server-2.0.3.tar.gz
cd nacos
sed -i 's/# db.num=1/db.num=1/' conf/application.properties
sed -i 's/# db.user.0=nacos/db.user.0=root/' conf/application.properties
sed -i 's/# db.password.0=nacos/db.password.0=root/' conf/application.properties

# sed -i 's/${MYSQL_OLD_CONF}/${MYSQL_NEW_CONF}/g' conf/application.properties
sed -i 's/# db.url.0=jdbc:mysql:\/\/127.0.0.1:3306\/nacos/db.url.0=jdbc:mysql:\/\/localhost:3306\/nacos_config/' conf/application.properties


# 启动 Nacos 服务器
cd bin
bash startup.sh -m standalone

